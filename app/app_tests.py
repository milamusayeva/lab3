import pytest
from fastapi.testclient import TestClient
import sys
sys.path.append("/code/app")
from main import app

client = TestClient(app)

@pytest.mark.asyncio
async def test_create_user_endpoint():
    user_data = {"username": "testuser", "password": "testpass"}
    response = client.post("/users/", json=user_data)
    assert response.status_code == 200
    created_user = response.json()
    assert created_user["username"] == user_data["username"]

@pytest.mark.asyncio
async def test_read_user_endpoint():
    response = client.get("/users/1")
    assert response.status_code == 200
    user = response.json()
    assert "username" in user

@pytest.mark.asyncio
async def test_update_user_endpoint():
    user_id = 1
    user_update_data = {"username": "updated_username", "password": "updated_pass"}
    response = client.put(f"/users/{user_id}", json=user_update_data)
    assert response.status_code == 200
    updated_user = response.json()
    assert updated_user["username"] == user_update_data["username"]

@pytest.mark.asyncio
async def test_delete_user_endpoint():
    user_id = 1
    response = client.delete(f"/users/{user_id}")
    assert response.status_code == 200
