USE user_db;

CREATE TABLE User (
                      id INT AUTO_INCREMENT PRIMARY KEY,
                      username VARCHAR(45) NOT NULL,
                      password CHAR(64) NOT NULL
);
