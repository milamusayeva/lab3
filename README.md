# DevOPS Lab 3 - Ansible, Docker, Nginx, GitlabCI

## [Access the application here](http://158.180.21.20/docs)

## Overview
This project utilizes a CI/CD pipeline, Docker for containerization, and Ansible for automation, targeting server and database configuration.

## Files Description

### CI/CD Configuration
- `gitlab-ci.yml`: Defines the stages for continuous integration and deployment, utilizing Docker and Ansible.

### Docker Configuration
- `Dockerfile`: Creates a Docker image for a Python-based application, installing necessary dependencies.

### Ansible Playbooks
- `infra-playbook.yml`: Configures MySQL, Docker, and NGINX on servers.
- `deploy_playbook.yml`: Deploys the application on the development server.
- `build_playbook.yml`: Builds and pushes the Docker image to a registry.

### Ansible Roles and Tasks
- `roles/application_crud/tasks/main.yml`: Manages Docker containers for the application.
- `roles/application_crud/tasks/docker_tasks.yml`: Handles Docker container operations.
- `roles/custom_mysql/tasks/main.yml`: Sets up and configures the MySQL server.

### Configuration Templates
- `templates/default.conf.j2`: NGINX configuration template using Jinja2.

## Environment and Dependencies
Ensure Docker, Ansible, and Python are installed on your system for running these configurations and playbooks.

## Usage

### 1. Prepare Your Environment
```bash
ansible --version  # Ensure Ansible is installed
docker --version   # Ensure Docker is installed
# Clone your project repository
```

### 2. Set Up Variables
- Update variables in playbooks and roles with your server details and credentials.

### 3. Run Infrastructure Playbook
```bash
cd [your project directory]
ansible-playbook infra-playbook.yml -i [your inventory file]
```

### 4. Run Build Playbook
```bash
ansible-playbook build_playbook.yml -i [your inventory file]
```

### 5. Run Deployment Playbook
```bash
ansible-playbook deploy_playbook.yml -i [your inventory file]
```

### 6. Access the Application
- Access your application at `http://your-server-ip` or `http://your-domain-name`.

### 7. Verify Application Status
- Check the application logs or server status to ensure it's running correctly.

## Notes
- The NGINX configuration can be modified in `default.conf.j2` based on your requirements.
